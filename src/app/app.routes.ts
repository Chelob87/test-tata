import { Routes } from '@angular/router';
import { FinancialProductsComponent } from './financial-products/financial-products.component';
import { ProductComponent } from './product/product.component';

export const routes: Routes = [
    { path: '', component: FinancialProductsComponent },
    { path: 'product', component: ProductComponent }
];
