import { Component } from '@angular/core';
import { BaseService } from '../services/Base.servise';
import { CommonModule, formatDate } from '@angular/common';
import { FormsModule } from '@angular/forms';

export interface FinancialProductsData {
  id: string;
  name: string;
  description: string;
  logo: string;
  date_release: Date;
  date_revision: Date;
}

@Component({
  selector: 'app-financial-products',
  standalone: true,
  imports: [CommonModule, FormsModule],
  providers: [BaseService],
  templateUrl: './financial-products.component.html',
  styleUrl: './financial-products.component.scss'
})

export class FinancialProductsComponent {
  dataSource : FinancialProductsData[] = [];
  filteredProducts : FinancialProductsData[] = [];
  filterKey : string = '';
  totalResults: number = 0;
  activeSubmenu: string = "";
  constructor(private service : BaseService) { };
  
  customFormattDate(date: Date) {
    return formatDate(date, 'dd/MM/yyyy', 'en-US')
  }
  
  filterChangeFn(value: string) {
    this.filterKey = value;
    this.filterData();
  }

  filterData() {
    if(this.filterKey.length === 0) {
      this.filteredProducts = this.dataSource;
    } else {
      this.filteredProducts = this.dataSource.filter(product => product.description.includes(this.filterKey) || product.name.includes(this.filterKey)
      );
    }
    this.totalResults = this.filteredProducts.length;
  }

  handleMenuClick(id: string) {
    if(this.activeSubmenu === id){
      this.activeSubmenu = "id";
    }else {
      this.activeSubmenu = id;
    }
  }

  handleDelete(id: string) {
    this.service.deleteProduct(id).subscribe({
      error: (error) => {
        this.handleGetProducts();
      }
    })
  }

  handleGetProducts() {
    this.service.getProducts().subscribe({
      error: (error) => {
        console.log(error);
      },
      next: (data) => {
        this.dataSource = data;
        this.filteredProducts = data;
        this.totalResults = data.length;
      }
    })
  }

  ngOnInit(): void {
    this.handleGetProducts();
  }
}
