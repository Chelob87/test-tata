import { Component } from '@angular/core';
import { BaseService } from '../services/Base.servise';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

export interface FinancialProductsData {
  id: string;
  name: string;
  description: string;
  logo: string;
  date_release: Date;
  date_revision: Date;
}

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [CommonModule, FormsModule],
  providers: [BaseService],
  templateUrl: './product.component.html',
  styleUrl: './product.component.scss'
})
export class ProductComponent {
  id:string = "";
  name: string = "";
  description: string = "";
  logo: string = "";
  relDate: Date | null = null;
  revDate: Date | null = null;


  handleSubmit(e: Event) {
    console.log(e);
  }
}
