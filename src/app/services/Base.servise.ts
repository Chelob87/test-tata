import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }    from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class BaseService {
    constructor(private httpClient: HttpClient) { }

    public getProducts() : Observable<any>{
        const url = 'https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products';
        const headers = new HttpHeaders().set("authorId", "1");
        return this.httpClient.get(url, { headers });
    }

    public deleteProduct(id: string) : Observable<any>{
        const url = `https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/ipf-msa-productosfinancieros/bp/products?id=${id}`;
        const headers = new HttpHeaders().set("authorId", "1").set('Content-Type', 'application/json');
        return this.httpClient.delete(url, { headers });
    }

}